var nodes = [];
var pause = false;

function setup()
{
    var cnv = createCanvas(windowWidth, windowHeight);

    //frameRate(30);

    for (var i = 0; i < 100; i++)
        nodes.push(new Node());
}

function draw()
{
    if (!pause)
    {
        background(192);

        nodes.forEach(function(node, index)
        {
            if (node.is_outside())
                nodes[index] = new Node();

            node.update();

            for (var i = index + 1; i < nodes.length; i++)
            {
                var second = nodes[i];

                var alpha = node.is_close(second);
                if (alpha < 128)
                {
                    stroke(0, 128 - alpha);
                    line(node.x, node.y, second.x, second.y);
                }
            }

            node.draw();
        });
    }
}

function keyPressed()
{
    if (keyCode === 80)
        pause = !pause;
}

function windowResized()
{
    resizeCanvas(windowWidth, windowHeight);
}

var Node = function()
{
    var h = parseInt(Math.random() * 2);

    this.x          = h === 0 ? parseInt(Math.random() * width) : parseInt(Math.random() * 2) * width;
    this.y          = h === 1 ? parseInt(Math.random() * height) : parseInt(Math.random() * 2) * height;
    this.speed      = 1 + parseInt(Math.random() * 2);
    this.diameter   = 5 + parseInt(Math.random() * 10);
    this.angle      = parseInt(Math.random() * 360);
    this.quadrant   = parseInt(this.angle / 90) + 1;

    if (
        (this.x === 0 && (this.quadrant === 2 || this.quadrant === 3)) ||
        (this.x === width && (this.quadrant === 1 || this.quadrant === 4)) ||
        (this.y === 0 && (this.quadrant === 1 || this.quadrant === 2)) ||
        (this.y === height && (this.quadrant === 3 || this.quadrant === 4))
    )
        this.angle = (this.angle + 180) % 360;

    this.quadrant = parseInt(this.angle / 90) + 1;

    if (this.x === 0)
        this.x = -this.diameter;
    if (this.x === width)
        this.x += this.diameter;

    if (this.y === 0)
        this.y = -this.diameter;
    if (this.y === height)
        this.y += this.diameter;

    this.update = function()
    {
        var rad = this.angle * Math.PI / 180.0;
        var cos = Math.cos(rad);
        var sin = Math.sin(rad);

        var x = this.speed * cos;
        var y = this.speed * sin;

        this.x += x;
        this.y -= y;
    };

    this.is_outside = function()
    {
        return this.x < -this.diameter || this.x > width + this.diameter || this.y < -this.diameter || this.y > height + this.diameter;
    };

    this.draw = function()
    {
        noStroke();
        fill(132, 87, 25);
        ellipse(this.x, this.y, this.diameter, this.diameter);
    };

    this.is_close = function(node)
    {
        return Math.sqrt(Math.pow(this.y - node.y, 2) + Math.pow(this.x - node.x, 2));
    }
}